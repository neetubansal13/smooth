package com.thinkers.smooth.users;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/roles")
public class RoleController {
	@Autowired RoleService roleService;
	@GetMapping
	public List<Role> getRoles(){
		return roleService.getAllRoles();
	}
}
