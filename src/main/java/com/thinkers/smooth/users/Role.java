package com.thinkers.smooth.users;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Role {

	@Id
	private Long roleId;
	private String role;
	
	public Role() {
		super();
	}
	
	public Role(Long roleId, String role) {
		super();
		this.roleId = roleId;
		this.role = role;
	}

	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	@Override
	public String toString() {
		return "Role [roleId=" + roleId + ", role=" + role + "]";
	}
	
	
	
}
