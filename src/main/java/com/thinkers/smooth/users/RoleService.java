package com.thinkers.smooth.users;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

	@Autowired RoleRepository roleRepo;
	
	public List<Role> getAllRoles(){
		return (List<Role>) roleRepo.findAllByRoleNotIn("MASTER");
	}
}
