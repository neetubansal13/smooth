package com.thinkers.smooth.users;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role,Long> {

public List<Role> findAllByRoleNotIn(String role);
}
