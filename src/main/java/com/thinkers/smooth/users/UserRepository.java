package com.thinkers.smooth.users;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,Long>{

	@Query(value="SELECT u.* FROM USER u INNER JOIN role r ON r.role_id=u.role_id WHERE u.company_id=? AND r.role!='ADMIN'",nativeQuery=true)
	public List<User> getUsersByCompanyIdAndRole(Long companyId);
	
	public List<User> findByUserName(String emailId);
	

}
