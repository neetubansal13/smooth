package com.thinkers.smooth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/")
@RestController
@SpringBootApplication
public class SmoothApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmoothApplication.class, args);
	}

	@GetMapping	
	public String giveSimpleMessage (){
		return "Yes Its a working application!!!!!!!";
	}
	
	
	@GetMapping("/message")
	public String giveSimpleMessage2 (){
		return "This is another message from my application.";
	}
}
